/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controlador;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.primefaces.event.FileUploadEvent;

/**
 *
 * @author rodas
 */
@ManagedBean
@ViewScoped
public class SubidaControlador implements Serializable {

    private static final String RUTA = File.separator + "home" + File.separator + "rodas" + File.separator + "code" + File.separator + "img" + File.separator;
    public String destino;
    public List<String> archivos;

    public List<String> getArchivos() {
        return archivos;
    }

    public void setArchivos(List<String> archivos) {
        this.archivos = archivos;
    }
    
    public String getDestino() {
        return destino;
    }

    public void setDestino(String destino) {
        this.destino = destino;
    }

    public SubidaControlador() {
    }

    @PostConstruct
    public void init() {
        System.out.println("com.controlador.SubidaControlador.init()");
    }

    public void handleFileUpload(FileUploadEvent event) throws IOException {
        System.out.println("handleFileUpload antes del try ");
        try {
            System.out.println("handleFileUpload en el try ");

            destino = copyFile(RUTA, event.getFile().getFileName(), event.getFile().getInputstream());
            mensajes(FacesMessage.SEVERITY_INFO, "Operación con Exito!", "");
        } catch (IOException ex) {
            System.out.println("com.controlador.SubidaControlador.handleFileUpload()");
            mensajes(FacesMessage.SEVERITY_ERROR, "El archivo no pudo copiarse" + ex, "");
        }
    }

    public void guardar() {
        System.out.println("com.controlador.SubidaControlador.guardar()");
        
        File archivosImg = new File(RUTA);
        archivos = new ArrayList<>();
        for (String s : archivosImg.list()) {
            archivos.add(s);
        }
    }

    /**
     * El metodo realizza la copia de los archivos a una ruta en especifico,
     * primero de le debe de asignar la rutaArchivo
     *
     * @param rutaArchivo
     * @param fileName nombre del archivo
     * @param in el InputStream
     * @return retorna la ruta de la cual se copio
     * @throws java.io.FileNotFoundException
     *
     */
    public static String copyFile(String rutaArchivo, String fileName, InputStream in) throws FileNotFoundException {
        System.out.println("copyFile " + rutaArchivo);
        try {
            System.out.println("copyFile en el try");

            OutputStream out = new FileOutputStream(new File(rutaArchivo + fileName));
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = in.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
            in.close();
            out.flush();
            out.close();
        } catch (IOException e) {
            System.out.println("com.controlador.SubidaControlador.copyFile()  " + e);
        }
        return rutaArchivo + fileName;
    }

    /**
     * Metodo que realiza un mensaje para las etiquetas de JSF o PrimeFaces.
     *
     * @param severity Indica el tipo de mensaje.
     * @param detail mensaje de tipo detalle.
     * @param summary mensaje de tipo principal.
     */
    public void mensajes(FacesMessage.Severity severity, String summary, String detail) {
        FacesMessage fm = new FacesMessage(severity, summary, detail);
        FacesContext.getCurrentInstance().addMessage(null, fm);
    }

}
